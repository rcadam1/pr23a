public class tenista 
{
	private String name;
	private int grandSlams;
	
	//constructores
	public tenista()
	{
		name="Vacío";
		grandSlams=0;
	}
	
	public tenista(String name, int grandSlams)
	{
		this.name=name;
		this.grandSlams=grandSlams;
	}
	
	public tenista(tenista t)
	{
		name=t.name;
		grandSlams=t.grandSlams;
	}
	
	//setters
	public void setName(String n)
	{
		name=n;
	}
	
	public void setGrandSlams(int g)
	{
		grandSlams=g;
	}
	
	//getters
	public String getName()
	{
		return name;
	}
	
	public int getGrandSlams()
	{
		return grandSlams;
	}
	
	public void printPlayer()
	{
		System.out.println("El tenista "+name+" tiene "+grandSlams+" grand slams");
	}
	
}
